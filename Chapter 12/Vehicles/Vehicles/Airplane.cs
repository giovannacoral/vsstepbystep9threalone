﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vehicles
{
	class Airplane : Vehicle // Airplane IS-A Vehicle
	{
		public Airplane(string make) : base(make)
		{ 
		}

		public override void Go()
		{
			Console.WriteLine("Neeooowwwww goes the plane");
		}
	}
}
